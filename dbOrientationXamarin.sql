-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:51154
-- Generation Time: May 09, 2019 at 12:15 AM
-- Server version: 5.7.9-log
-- PHP Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `localdb`
--
CREATE DATABASE IF NOT EXISTS `localdb` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `localdb`;

-- --------------------------------------------------------

--
-- Table structure for table `coordination`
--

CREATE TABLE `coordination` (
  `id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `latitude` double NOT NULL,
  `longitude` double NOT NULL,
  `id_Participant` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `coordination`
--

INSERT INTO `coordination` (`id`, `date`, `latitude`, `longitude`, `id_Participant`) VALUES
(9, '2019-03-13 00:00:00', 55, 68, 27),
(10, '2019-03-13 16:23:37', 55, 68, 9),
(13, '2019-03-13 18:31:56', 55, 68, 25),
(14, '2019-03-13 16:31:59', 55, 68, 26),
(15, '2019-03-13 16:32:09', 55, 68, 39),
(16, '2019-03-13 17:01:26', 55, 68, 38),
(17, '2019-03-13 17:20:12', 60, 68, 38),
(18, '2019-03-13 17:20:19', 75, 50, 39),
(19, '2019-03-13 17:20:25', 100, 100, 39),
(44, '2019-03-20 14:27:28', 50, 50, 40),
(46, '2019-03-20 14:28:47', 37.4219983333333, -122.084, 40),
(54, '2019-03-20 14:57:50', 52, 78, 40),
(56, '2019-03-20 15:15:32', 35, 100, 40),
(63, '2019-03-20 15:35:38', 41, 15.5, 40),
(64, '2019-03-20 15:36:46', 17.1999983333333, 15.5, 40),
(65, '2019-03-20 15:37:20', 37.4219983333333, -122.084, 40),
(66, '2019-03-20 15:37:20', 37.4219983333333, -122.084, 40),
(67, '2019-03-20 15:37:55', 54.1899983333333, 31.6299983333333, 40),
(68, '2019-03-20 15:38:47', 60.5399983333333, 54.1799983333333, 40),
(69, '2019-03-20 15:39:18', 58.32, 12.5199983333333, 40),
(70, '2019-03-20 15:39:49', 65.2579983333333, 54.35, 40),
(71, '2019-03-20 15:46:35', 65.2579983333333, 54.35, 40),
(72, '2019-03-20 15:47:13', 84.25, 53.3599983333333, 40),
(73, '2019-03-20 16:05:01', 37.4219983333333, -122.084, 40),
(74, '2019-03-20 16:05:37', 37.4219983333333, -122.084, 40),
(75, '2019-03-20 16:07:42', 37.4219983333333, -122.084, 40),
(76, '2019-03-20 16:14:29', 84.25, 53.3599983333333, 40),
(77, '2019-03-20 16:15:48', 37.4219983333333, -122.084, 40),
(78, '2019-03-20 16:20:03', 84.25, 53.3599983333333, 40),
(79, '2019-03-20 16:22:20', 37.4219983333333, -122.084, 40),
(80, '2019-03-20 16:30:25', 84.25, 53.3599983333333, 40),
(81, '2019-04-25 12:28:18', 84.25, 53.3599983333333, 40),
(82, '2019-04-30 07:10:22', 84.25, 53.3599983333333, 40);

-- --------------------------------------------------------

--
-- Table structure for table `course`
--

CREATE TABLE `course` (
  `date_depart` varchar(50) NOT NULL,
  `date_arrivee` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `course`
--

INSERT INTO `course` (`date_depart`, `date_arrivee`) VALUES
('2018-10-17 14:48:29', '2018-10-17 14:50:27');

-- --------------------------------------------------------

--
-- Table structure for table `equipe`
--

CREATE TABLE `equipe` (
  `id` int(11) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `couleur` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `equipe`
--

INSERT INTO `equipe` (`id`, `nom`, `couleur`) VALUES
(2, 'Equipe1', 'yellow_dot'),
(6, 'Equipe2', 'green_dot'),
(7, 'Equipe de Tutu', 'lightblue_dot'),
(8, 'Equipe de toto', 'orange_dot'),
(9, 'Equipe de titi', 'red_dot'),
(10, 'Equipe8', 'red_dot'),
(11, '2', '');

-- --------------------------------------------------------

--
-- Table structure for table `participant`
--

CREATE TABLE `participant` (
  `id` int(11) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `prenom` varchar(50) NOT NULL,
  `mail` varchar(50) NOT NULL,
  `age` int(11) NOT NULL,
  `telephone` varchar(50) NOT NULL,
  `sexe` varchar(50) NOT NULL,
  `id_Equipe` int(11) NOT NULL,
  `password` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `participant`
--

INSERT INTO `participant` (`id`, `nom`, `prenom`, `mail`, `age`, `telephone`, `sexe`, `id_Equipe`, `password`) VALUES
(9, 'Louis', 'Pierre', 'louispierre@gmail.com', 2, '0', 'sexe2', 2, '1234'),
(24, 'Jean', 'Christophe', 'em@em.fr', 18, '06859999', 'M', 7, '1234'),
(25, 'Paul', 'Jacque', 'toto1@toto.com', 1, '0666666', 'sexe1', 8, '1234'),
(26, 'Michel', 'Luc', 'titi@tofto.fr', 1, '666666', 'sexe1', 8, '1234'),
(27, 'Tito', 'Marc', 'toto1@toto.fr', 2, '666667', 'sexe2', 9, '1234'),
(38, 'titi', 'Michelle', 'toto2@toto.fr', 1, '300000', 'femme', 11, '1234'),
(39, 'toto', 'Jean-Paul', 'toto3@toto.fr', 1, '300000', 'homme', 11, '1234'),
(40, 'titi', 'Valentin', 'titi@toto.fr', 1, '300000', 'femme', 11, '1234');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `coordination`
--
ALTER TABLE `coordination`
  ADD PRIMARY KEY (`id`),
  ADD KEY `coordination_Participant_FK` (`id_Participant`);

--
-- Indexes for table `course`
--
ALTER TABLE `course`
  ADD PRIMARY KEY (`date_depart`);

--
-- Indexes for table `equipe`
--
ALTER TABLE `equipe`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `participant`
--
ALTER TABLE `participant`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `mail` (`mail`),
  ADD KEY `Participant_Equipe_FK` (`id_Equipe`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `coordination`
--
ALTER TABLE `coordination`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=83;
--
-- AUTO_INCREMENT for table `equipe`
--
ALTER TABLE `equipe`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `participant`
--
ALTER TABLE `participant`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `coordination`
--
ALTER TABLE `coordination`
  ADD CONSTRAINT `coordination_Participant_FK` FOREIGN KEY (`id_Participant`) REFERENCES `participant` (`id`);

--
-- Constraints for table `participant`
--
ALTER TABLE `participant`
  ADD CONSTRAINT `Participant_Equipe_FK` FOREIGN KEY (`id_Equipe`) REFERENCES `equipe` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
