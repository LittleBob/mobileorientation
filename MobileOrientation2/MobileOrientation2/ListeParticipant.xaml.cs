﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MobileOrientation2.classes;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Newtonsoft.Json;
using System.Net.Http.Headers;
using System.Net.Http;
using System.Net;
using System.IO;

namespace MobileOrientation2
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ListeParticipant : ContentPage
	{
        public static ObservableCollection<Participant> lesParticipants;
        public ListeParticipant ()
		{
            
            InitializeComponent ();
            var minute = TimeSpan.FromSeconds(60);
            Device.StartTimer(minute, () => { return false; });
            lesParticipants = new ObservableCollection<Participant>();
            ParticipantView.ItemsSource = lesParticipants;
            DisplayAlert("TEST", "TESTTT", "OK");
            //ParticipantView.ItemSelected += ParticipantView_ItemSelected;
            
        }
        
        void OnButtonVisualiserClicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new Map(Convert.ToInt32((sender as Button).CommandParameter.ToString())));
            //DisplayAlert("T", (sender as Button).CommandParameter.ToString(), "OK");
        }

        void OnButtonListeClicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new Map());
        }
    }
}