﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MobileOrientation2.classes
{
    public class Coordonnees
    {
        int id;
        string date;
        double latitude;
        double longitude;
        int id_Participant;

        public Coordonnees(int id, string date, double latitude, double longitude, int id_participant)
        {
            this.id = id;
            this.date = date;
            this.latitude = latitude;
            this.longitude = longitude;
            this.id_Participant = id_participant;
        }

        public int Id { get => id; set => id = value; }
        public string Date { get => date; set => date = value; }
        public double Latitude { get => latitude; set => latitude = value; }
        public double Longitude { get => longitude; set => longitude = value; }
        public int Id_Participant { get => id_Participant; set => id_Participant = value; }
    }
}
