﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MobileOrientation2.classes
{
    public class Participant
    {
        private int id;
        private string nom;
        private string prenom;
        private string mail;
        private int age;
        private string telephone;
        private string sexe;
        private int id_Equipe;
        private string password;
        private double latitude;
        private double longitude;

        public Participant(int id, string nom, string prenom, string mail, int age, string telephone, string sexe, int id_Equipe, string password, double latitude, double longitude)
        {
            this.id = id;
            this.nom = nom;
            this.prenom = prenom;
            this.mail = mail;
            this.age = age;
            this.telephone = telephone;
            this.sexe = sexe;
            this.id_Equipe = id_Equipe;
            this.password = password;
            this.latitude = latitude;
            this.longitude = longitude;
        }

       /* public Participant()
        {

        }*/

        public int Id { get => id; set => id = value; }
        public string Nom { get => nom; set => nom = value; }
        public string Prenom { get => prenom; set => prenom = value; }
        public string Mail { get => mail; set => mail = value; }
        public int Age { get => age; set => age = value; }
        public string Telephone { get => telephone; set => telephone = value; }
        public string Sexe { get => sexe; set => sexe = value; }
        public int Id_Equipe { get => id_Equipe; set => id_Equipe = value; }
        public string Password { get => password; set => password = value; }
        public double Latitude { get => latitude; set => latitude = value; }
        public double Longitude { get => longitude; set => longitude = value; }
    }
}
