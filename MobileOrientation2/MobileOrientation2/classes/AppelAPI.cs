﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Diagnostics;
using Newtonsoft.Json;

namespace MobileOrientation2.classes
{
    class AppelAPI
    {
        HttpClient client;

        public AppelAPI() { client = new HttpClient(); }
        public string url = "https://apiorientation.azurewebsites.net/api/ApiO/";

        public async Task<List<Participant>> loadAllContacts(int id)
        {
            string RestUrl = url + "participant/" + id;
            var uri = new Uri(string.Format(RestUrl, string.Empty));
            List<Participant> items = new List<Participant>();
            try
            {
                var response = await client.GetAsync(uri);

                if (response.IsSuccessStatusCode) {
                    var content = await response.Content.ReadAsStringAsync();
                    items = JsonConvert.DeserializeObject<List<Participant>>(content);
                    return items;
                } else {
                    return null;
                }
            }
            catch (Exception e) { Debug.WriteLine(e.Message); return null; }
        }
        public async Task<List<Coordonnees>> loadAllCoordonne(int idParticipant)
        {
            string RestUrl = url+"coordonnees/"+idParticipant;
            var uri = new Uri(string.Format(RestUrl, string.Empty));
            List<Coordonnees> items = new List<Coordonnees>();
            try
            {
                var response = await client.GetAsync(uri);

                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    items = JsonConvert.DeserializeObject<List<Coordonnees>>(content);
                    return items;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception e) { Debug.WriteLine(e.Message); return null; }
        }
        public async Task<List<Coordonnees>> loadAllCoordonne()
        {
            string RestUrl = url + "allcoordonnees/" + LoginPage.IdCurrentParticipant;
            var uri = new Uri(string.Format(RestUrl, string.Empty));
            List<Coordonnees> items = new List<Coordonnees>();
            try
            {
                var response = await client.GetAsync(uri);

                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    items = JsonConvert.DeserializeObject<List<Coordonnees>>(content);
                    return items;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception e) { Debug.WriteLine(e.Message); return null; }
        }
        public async Task<int> login(Participant p)
        {

            string RestUrl = url;
            var uri = new Uri(string.Format(RestUrl, string.Empty));

            var json = JsonConvert.SerializeObject(p);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            HttpResponseMessage response = null;
            try
            {
                response = await client.PostAsync(uri, content);
                if (response.IsSuccessStatusCode)
                {
                    var pageContent = await response.Content.ReadAsStringAsync();
                    var id = JsonConvert.DeserializeObject<int>(pageContent);
                    return id;
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception e) { Debug.WriteLine(e.Message); return 0; }
        }

        public async void sendCoordonnees(Coordonnees c)
        {
            string RestUrl = url;
            var uri = new Uri(string.Format(RestUrl, string.Empty));

            var json = JsonConvert.SerializeObject(c, new JsonSerializerSettings
            {
                // tr culture separator is ","..
                Culture = new System.Globalization.CultureInfo("en-US")  //Replace tr-TR by your own culture
            });
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            HttpResponseMessage response = null;
            try
            {
                response = await client.PutAsync(uri, content);
            }
            catch (Exception e) { Debug.WriteLine(e.Message); }
        }
    }
}
