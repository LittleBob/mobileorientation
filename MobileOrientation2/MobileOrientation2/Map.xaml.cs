﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Xaml;
using MobileOrientation2.classes;
using System.Collections.ObjectModel;

namespace MobileOrientation2
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Map : ContentPage
	{
        public static ObservableCollection<Coordonnees> lesCoordonnees;
        public List<Pin> test;
        public static double longitude;
        public static double latitude;
        private int idP;

        public Map (int idParticipant)
		{
			InitializeComponent ();
            lesCoordonnees = new ObservableCollection<Coordonnees>();
            lesCoordonnees.Clear();
            test = new List<Pin>();
            getAllCoordonnees(idParticipant);
            idP = idParticipant;
            var minute = TimeSpan.FromSeconds(60);
            Device.StartTimer(minute, () => { getAllCoordonnees(idParticipant); return true; });
        }
        public Map()
        {
            InitializeComponent();
            lesCoordonnees = new ObservableCollection<Coordonnees>();
            lesCoordonnees.Clear();
            test = new List<Pin>();
            getAllCoordonnees();
            var minute = TimeSpan.FromSeconds(60);
            Device.StartTimer(minute, () => { getAllCoordonnees(); return true; });
        }
        void Afficher()
        {
            MyMap.Pins.Clear();
            foreach (Coordonnees item in lesCoordonnees)
            {
                foreach (Participant p in ListeParticipant.lesParticipants)
                {
                    if (p.Id == item.Id_Participant)
                    {
                        var position = new Position(item.Latitude, item.Longitude); // Latitude, Longitude        
                        var pin = new Pin
                        {
                            Type = PinType.Generic,
                            Position = position,
                            Label = p.Nom + " " + p.Prenom,
                            Address = "Position n° " + MyMap.Pins.Count + 1
                        };
                        MyMap.Pins.Add(pin);
                    }
                }
            }
            lesCoordonnees.Clear();
        }
        void OnButtonValiderClicked(object sender,EventArgs e)
        {
            getAllCoordonnees(idP);
        }
        void OnButtonListeClicked(object sender,EventArgs e)
        {
            DisplayAlert("T", lesCoordonnees.Count.ToString() + " PINS : " +  MyMap.Pins.Count, "ok");
        }
        public async Task<List<Coordonnees>> AppelGet(int idParticipant)
        {
            AppelAPI rsrv = new AppelAPI();
            return await rsrv.loadAllCoordonne(idParticipant);
        }
        public async void getAllCoordonnees(int idParticipant)
        {
            List<Coordonnees> lesC = await AppelGet(idParticipant);
            foreach (Coordonnees c in lesC)
            {
                Map.lesCoordonnees.Add(c);
            }
            Afficher();
        }


        public async Task<List<Coordonnees>> AppelGet()
        {
            AppelAPI rsrv = new AppelAPI();
            return await rsrv.loadAllCoordonne();
        }
        public async void getAllCoordonnees()
        {
            List<Coordonnees> lesC = await AppelGet();
            foreach (Coordonnees c in lesC)
            {
                Map.lesCoordonnees.Add(c);
            }
            Afficher();
        }
    }
}