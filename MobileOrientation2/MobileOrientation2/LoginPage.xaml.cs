﻿using MobileOrientation2.classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plugin.Geolocator;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Diagnostics;

namespace MobileOrientation2
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LoginPage : ContentPage
	{
        bool test = false;
        public static Plugin.Geolocator.Abstractions.IGeolocator locator;
        public static int IdCurrentParticipant;

        public static bool envoye = false;

        AppelAPI currentAPI = new AppelAPI();
        public LoginPage ()
		{
			InitializeComponent ();
		}
        async void OnButtonConnectEvent(object sender, EventArgs e)
        {
            if (!test)
            {
                AppelAPI t = new AppelAPI();
                Participant p = new Participant(0, "", "", "", 0, "", "", 0, "", 0, 0);
                test = true;
                if (entryPwd.Text.Length > 0 && entryMail.Text.Length > 0)
                {
                    p.Mail = entryMail.Text;
                    p.Password = entryPwd.Text;
                    labelError.Text = "En cours de connexion";
                    int id = await t.login(p);
                    if ( id != 0)
                    {
                        getAllParticipant(id);
                        IdCurrentParticipant = id;
                        await Navigation.PushAsync(new ListeParticipant());
                        await StartListening();                      
                    }
                    else
                    {
                        labelError.Text = "Email ou mot de passe incorrect";
                        test = false;
                    }
                }
                else
                {
                    labelError.Text = "Merci de rentrer vos identifiants de connexion";
                    test = false;
                }
            }
        }
        public static async Task<List<Participant>> AppelGet(int id)
        {
            AppelAPI currentAPI = new AppelAPI();
            return await currentAPI.loadAllContacts(id);
        }
        public async void getAllParticipant(int id)
        {
            List<Participant> lesC = await AppelGet(id);
            foreach (Participant c in lesC)
            {
                ListeParticipant.lesParticipants.Add(c);
            }
        }

        async Task StartListening()
        {
            if (CrossGeolocator.Current.IsListening)
                return;

            ///This logic will run on the background automatically on iOS, however for Android and UWP you must put logic in background services. Else if your app is killed the location updates will be killed.
            await CrossGeolocator.Current.StartListeningAsync(TimeSpan.FromSeconds(5), 10, true, new Plugin.Geolocator.Abstractions.ListenerSettings
            {
                ActivityType = Plugin.Geolocator.Abstractions.ActivityType.AutomotiveNavigation,
                AllowBackgroundUpdates = true,
                DeferLocationUpdates = true,
                DeferralDistanceMeters = 1,
                DeferralTime = TimeSpan.FromSeconds(1),
                ListenForSignificantChanges = true,
                PauseLocationUpdatesAutomatically = false
            });

            CrossGeolocator.Current.PositionChanged += Current_PositionChanged;
        }
        private void Current_PositionChanged(object sender, Plugin.Geolocator.Abstractions.PositionEventArgs e)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                var test = e.Position;
                if (envoye == false)
                {
                    currentAPI.sendCoordonnees(new Coordonnees(0, "", test.Latitude, test.Longitude, IdCurrentParticipant));
                    envoye = true;
                }
                else
                {
                    envoye = false;
                }
            });
        }
    }
}